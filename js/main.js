var userinfo;

accessToken = Cookies.get("accessToken");
var spotifyApi = new SpotifyWebApi();
spotifyApi.setAccessToken(accessToken);

function getPromiseMergedPlaylist() {
  return getPromiseToMergePlaylistsFromDescriptionOfPlaylistById("3llqkp7ztxPUqxju1e6HX7")
  .then(function(data) {
    return data.map(function(d){
      return getPromiseSongTrackIdsFromPlaylistById(d);
    })
  }).then(function(toMergePlaylists) {
    return Promise.all(toMergePlaylists).then(function(values){
    //return (values);
    var union = [... new Set([...values[0], ...values[1]])];
    unionuris = union.map(function(item) {
      return "spotify:track:"+item;
    })
    return unionuris;
    })
  }).then(function(unionuris) {
    spotifyApi.addTracksToPlaylist("3llqkp7ztxPUqxju1e6HX7", unionuris);
  });
}

function getPromiseClearPlaylist (playlistId) {
  return getPromiseSongTrackIdsFromPlaylistById(playlistId)
  .then(function(data) {
    dmap = data.map(function(item) {
      return "spotify:track:"+item;
    });
    spotifyApi.removeTracksFromPlaylist(playlistId, dmap);
  }).then(function(retvalue) {
    return retvalue;
  });
}

function getPromiseToMergePlaylistsFromDescriptionOfPlaylistById (playlistId) {
  return spotifyApi.getPlaylist(playlistId)
  .then(function(data){
    let jsonObj = $("<div>").html(data["description"])[0].textContent;
    return JSON.parse(jsonObj);
  }).then(function(jsonObj) {
    return jsonObj["p"];
  })
}

function getPromiseSongTrackIdsFromPlaylistById(playlistId) {
  return spotifyApi.getPlaylistTracks(playlistId,
    {"market":"DE","fields":"items(track.id)"}
  )
  .then(function(data) {
    return data["items"].map(function(d){
      return d["track"]["id"];
    });
  });
}
