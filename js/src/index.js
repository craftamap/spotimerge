import URI from "urijs";
var Spotify = require('spotify-web-api-js');
var Cookies = require("js-cookie");
var $ = require("jquery");

/* jquery is globally available due for debugging*/
window.$ = $;

// gets accessToken from Cookies
var accessToken = Cookies.get("accessToken");

var spotifyApi = new Spotify();
spotifyApi.setAccessToken(accessToken);

window.spotifyApi = spotifyApi;
/*
 * LoginPopup
 * 
 * */
function loginPopup (url) {
  var popupWindow = window.open(
    url,'popUpWindow','height=600,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
    console.log(popupWindow);
}

$("#login").on("click", function() {
  loginPopup("https://accounts.spotify.com/authorize?client_id=122f5228c6eb45ad865922a575c4a998&scope=user-read-private%20user-read-email%20playlist-read-collaborative%20playlist-modify-public%20playlist-read-private%20playlist-modify-private&response_type=token&state=123&redirect_uri=http:%2F%2Fprojects.siegelfabian.de%2Fspotimerge%2Fauth-flow-response.html");
});

/*
 * returns Promise of all Playlists of the current user containing "#SpotiMerge" in their title
 *
 * */
function getPromiseExistingPlaylists() {
    return spotifyApi.getUserPlaylists({"limit": 50})
    .then(function(data) {
        let mapped = data["items"].map(function(item) {
            return {"name": item["name"], "id":item["id"]};
        });
        let filtered = mapped.filter(function(ele){
            return ele["name"].includes("#SpotiMerge");
        });
        return filtered;
    })
}

/*
 * returns Promise of Information about a specific Playlist
 *
 * */
function getPromisePlaylistInformation(playlistId) {
    return spotifyApi.getPlaylist(playlistId, {"fields":"name,id,description,tracks.items(track(uri,id,name,album(name),artists))"})
    .then(function (data) {
        return data;
    });
}


function getPromiseRecursiveFullPlaylistInformationStarter(playlistId) {
    return spotifyApi.getPlaylist(playlistId, {"fields":"name,id,description,tracks.items(track(uri,id,name,album(name),artists)), tracks.total, tracks.next, tracks.offset"})
    .then(function(data) {
        if(data.tracks.next == null) {
            return data;
        } else {
            return getPromiseRecursiveFullPlaylistInformationSub(data,data.tracks, playlistId);
        }
    });
}
/*
 * data = root object
 * dat = last caller
 * playlistId = playlistId
 * */
function getPromiseRecursiveFullPlaylistInformationSub(data,dat, playlistId) {
    /* if data.name is undefined, its a getPlaylistTracks, else its a getPlaylist*/
//    dat = (data.name == undefined) ? data : data.tracks;
    return spotifyApi.getPlaylistTracks(playlistId,{"fields":"items(track(uri,id,name,album(name),artists)),total, next, offset,limit", "offset":dat.offset+((data.name == undefined) ? dat.limit : 100)})
    .then(function(d) {
        data.tracks.items = data.tracks.items.concat(d.items);
        if (d.next == null) {
            return data;
        } else {
            return getPromiseRecursiveFullPlaylistInformationSub(data, d, playlistId);
        }
    });
} 


/*
 * binding for Button1A
 * gets existing Playlists, appends them to the Select2A, and shows step2A
 *
 * */

function bindingButton1A () {
    getPromiseExistingPlaylists()
    .then(function(items) {
        $.each(items, function(i,d) {
            //console.log(d);
             $("<option>").attr("value", d["id"]).text(d["name"]).appendTo("select[name='select2A']");
        })
        $("#step1").fadeOut(function(){$("#step2A").fadeIn()})
    })    
}

/*
 * binding for Button2A
 * checks, if an option is selected. If not, append warning. if, then Prepare next view with Playlist information.
 *
 * */

function bindingButton2A() {
    let val = $("#step2A select").val();
    if (val === null) {
        $("#step2A p").append("<span style='color:red'>Please select an Playlist.</span>");
    } else {
        //console.log(getPromisePlaylistInformation(val));
        refresh10A(val);
        
    }
}

function bindingSavePlaylistlistbutton10A(ev) {
    let e = ev.data;
    let uppl = parse10Aplaylistlist();
    let newDescription = e["json"];
    newDescription["p"] = parse10Aplaylistlist();
    let newDescrString = JSON.stringify(newDescription);
    console.log(newDescrString);
    spotifyApi.changePlaylistDetails(e["id"], {description: newDescrString})
    .finally(function (data) {
        refresh10A(e["id"]);
    });
    
}

/*
 * function to load and refresh step10A, gets called by button2a and on every chance in 10A
 *
 * */
function refresh10A(val) {
    getPromiseRecursiveFullPlaylistInformationStarter(val)
        .then( function(data){
            /* playlistlist */
            $("#step10A h3:first").html("<span>"+data["name"]+ "</span> - <span>"+ data["id"]+"</span>");
            let parsedDesc = JSON.parse($("<div>").html(data["description"])[0].textContent);
    
            playlistlist = parsedDesc["p"];
            appendGivenPlaylistsTo10Aplaylistlist(playlistlist);
            /* playlistcontent */
            console.log(data);
            let preparedForTable = data["tracks"]["items"].map(function(d,i,a) {
              return {
                  name: d["track"]["name"],
                  artist: d["track"]["artists"][0]["name"]
              }
            });
            console.log(preparedForTable);
            fill10ATablePreviewFromArray(preparedForTable);
            $("#step10A #save, #step10A #reset, #step10A #rebuild").off();
            $("#step10A #save").on("click", {
                id: val,
                json: parsedDesc
            }, bindingSavePlaylistlistbutton10A)

            $("#step10A #reset").on("click", {
                id: val,
            }, function (ev) {
                refresh10A(ev.data.id);
            })

            $("#step10A #rebuild").on("click", {
                id: val,
                json: parsedDesc,
                data: data
            }, function (ev) {
                binding10ARebuildButton(ev);
            })


            /* fade */
            $("#step2A").fadeOut(function(){$("#step10A").fadeIn()})
        }) 
}

/*
 * gets all Elements of 10Aplaylistlist and puts their values into an array, which gets returned 
 *
 * */
function parse10Aplaylistlist() {
    var yourArray = [];
    $("#step10A .playlistlistElem").each(function() {
        let variable = $(this).find("input[type=text]").val();
        if (variable == "") {
            
        } else {
            yourArray.push(variable);
        }
    })
    return yourArray;
}




/*
 * appens a list of elements as playlistslistElem's to the 10A Playlistlist
 * a playlistlistElem contains a readonly-text-input for the spotify-uri and a delete-button with binding
 *
 * */

function appendGivenPlaylistsTo10Aplaylistlist(playlistlist) {

    $(".playlistlistElem").remove();
    $.each(playlistlist, function(i,d) {
       let elem = $("<div></div>").addClass("playlistlistElem");
       $("<input>").addClass("playlistlistElemPlaylist").attr("type", "text").attr("value", d).attr("readonly", "readonly").appendTo(elem);
       $("<input>").addClass("playlistlistElemDelete").attr("type", "button").attr("value", "x").appendTo(elem);
       elem.find(".playlistlistElemDelete").on("click",elem, function(eventdata) {
            $(eventdata["data"][0]).remove();
       })
       $("#10AaddButton").before(elem);
   }) 
}

/*
 * Fills table of songs in 10A with input array
 *
 * */

function fill10ATablePreviewFromArray (arr) {
    $("tr:not(:first-child)").remove();
    $.each(arr, function(i,d){
        let elem = $("<tr></tr>");
        $("<td></td>").html(i).appendTo(elem);
        $("<td></td>").html(this["name"]).appendTo(elem);
        $("<td></td>").html(this["artist"]).appendTo(elem);
        elem.appendTo("#step10A table tbody");
    })
}

/*
 * returns a single Promise containing multiple Promises for filling up data
 *
 * */
function getPromiseOfPlaylistInfoOfMultiplePlaylistIds (arr) {
    let prom = arr.map(function(d,v,a) {
        return getPromiseRecursiveFullPlaylistInformationStarter(d);
    })
    return Promise.all(prom);
}


function getPromiseClearPlaylist (e, oldSongList) {
    let id = e.data["id"];
    /*let oldSongList = data["tracks"]["items"].map(function(d,i,a) {
        return d["track"]["uri"];
    }); */
    console.log(oldSongList);
    let toRemove = oldSongList.slice(0,100);
    var toPass = oldSongList.slice(100);
    console.log(toPass);
    return spotifyApi.removeTracksFromPlaylist(id, toRemove)
        .then(function(data) {
            if (toPass.length == 0) {
                return data;
            } else {
                return getPromiseClearPlaylist(e, toPass);
            }
        });
}

function getPromiseClearPlaylistChild (e, oldSonglist) {
    let id = e["id"];
    let data = e["data"];
    
    let toRemove = oldSongList.slice(0,100);
    let toPass = oldsongList.slice(100);
    
    return spotifyApi.removeTracksFromPlaylist(id, toRemove)
        .then(function(data) {
            if (toPass.length == 0) {
                return data;
            } else {
                return getPromiseClearPlaylistChild(e, toPass);
            }
        });

}

function getPromiseFillPlaylistWithArray (id, arr) {
    let toAdd = arr.slice(0,100);
    let toPass = arr.slice(100);
    return spotifyApi.addTracksToPlaylist(id, toAdd)
        .then(function(data) {
            if (toPass.length == 0) {
                return data;
            } else {
                return getPromiseFillPlaylistWithArray(id, toPass);
            }
        });
    
}

function MergePlaylists (arr) {
    return [...new Set([].concat(...arr))];
}

function binding10ARebuildButton (ev) {
    let songlistOld = ev.data.data.tracks.items;
    let mappedOld = songlistOld.map(function(d) {
        return d.track.uri;
    });


    getPromiseOfPlaylistInfoOfMultiplePlaylistIds(ev["data"]["json"]["p"])
    .then(function(data) {
        let mapped = data.map(function(d,i,a){
            return d.tracks.items.map(function(d2) {
                return d2.track.uri;
            })
        });


        var merged = MergePlaylists(mapped);
        console.log(merged);
        return getPromiseClearPlaylist(ev, mappedOld)
            .then(function(data) {
                return getPromiseFillPlaylistWithArray(ev.data.id, merged);
            })
    }).then(function() {
        refresh10A(e["id"]);
    });

}

/*
 * binding for 10AaddButton
 * Adds Logic to Button, pressing adds a new combination of uri-text-input and delete-button with binding
 * {"p":["4SKfZqjEa4ketU9g79DKAU","7tiDiqOHSSrTEP14VKRgzl"]}
 * */

function binding10AaddButtonLogic() {
    let elem = $("<div></div>").addClass("playlistlistElem");
    $("<input>").addClass("playlistlistElemPlaylist").attr("type", "text").attr("placeholder", "spotify-playlist-id...").appendTo(elem);
    $("<input>").addClass("playlistlistElemDelete").attr("type", "button").attr("value", "x").appendTo(elem);
    elem.find(".playlistlistElemDelete").on("click",elem, function(eventdata) {
        $(eventdata["data"][0]).remove();
    })
    $("#10AaddButton").before(elem);
}

$("#button1A").on("click", function () {
    bindingButton1A();
});

$("#button2A").on("click", function () {
    bindingButton2A();
});

$("#10AaddButton").on("click", function () {
    binding10AaddButtonLogic();
});


/*
 * provisional clone of the SpotifyParse Module
 * takes a string and parses it to the different formates
 *
 * */
var SpotifyParse = (function(){
    
    var parse = function(str) {
        if (RegExp("^[a-zA-Z0-9]+$").test(str)) {
            return returnMissing(str);
        } else {
            let q = new URL(str);
            if (q.protocol == "spotify:") {
                let split = q.pathname.split(":");
                if (split[0] == "artist") {
                    return returnSimple(split);
                } else if (split[0] == "track") {
                    return returnSimple(split);
                } else if (split[0] == "album") {
                    return returnSimple(split);
                    
                } else if (split[0] == "playlist") {
                    return returnSimple(split);
                    
                } else if (split[0] == "user") {
                    if (split[2] == undefined) {
                        return returnSimple(split);
                    } else if (split[2] == "playlist") {
                        return returnUserPlaylist(split);
                    }
                }

            } else if (q.protocol == "https:") {
                let split = q.pathname.split("/").slice(1);
                console.log(split);
                if (split[0] == "artist") {
                    return returnSimple(split);
                } else if (split[0] == "track") {
                    return returnSimple(split);
                } else if (split[0] == "album") {
                    return returnSimple(split);
                    
                } else if (split[0] == "playlist") {
                    return returnSimple(split);
                    
                } else if (split[0] == "user") {
                    if (split[2] == undefined) {
                        return returnSimple(split);
                    } else if (split[2] == "playlist") {
                        return returnUserPlaylist(split);
                    }
                }
            }
        }
    }

    var parseMissingWithGuess = function (str, guess) {
        return {
            type: guess,
            id: str,
            uri: "spotify:"+guess+":"+str,
            urlOpen: "https://open.spotify.com/"+guess+"/"+string
        }
    }

    /* var parseMissingWithTryAndErrorFunction function(str, sApi) {
        return {
            id: str,
            type: undefined,
            uri: undefined,
            urlOpen: undefined,
            TryAndError: function () {
            }
        }
    } */

    var returnMissing = function (str) {
        return {
            id: str,
            type: undefined,
            uri: undefined,
            urlOpen: undefined
        }

    }

    var returnSimple = function (arr) {
        return {
            type: arr[0],
            id: arr[1],
            uri: "spotify:"+arr[0]+":"+arr[1],
            urlOpen: "https://open.spotify.com/"+arr[0]+"/"+arr[1]
        };
    }

    var returnUserPlaylist = function (arr) {
        return {
            type: arr[2],
            id: arr[3],
            uri: "spotify:"+arr[0]+":"+arr[1]+":"+arr[2]+":"+arr[3],
            urlOpen: "https://open.spotify.com/"+arr[0]+"/"+arr[1]+"/"+arr[2]+"/"+arr[3]
        };
    }


    return {
        parse: parse,
    }
})();

window.SpotifyParse = SpotifyParse;

/**** PASTE AREA
 *
 * $("#step1").fadeOut(function(){$("#step2A").fadeIn()})
 *
 *
 *
 *
 *
 *
 */

/* Deprecated */
function getPromiseMergedPlaylist() {
  return getPromiseToMergePlaylistsFromDescriptionOfPlaylistById("3llqkp7ztxPUqxju1e6HX7")
  .then(function(data) {
    return data.map(function(d){
      return getPromiseSongTrackIdsFromPlaylistById(d);
    })
  }).then(function(toMergePlaylists) {
    return Promise.all(toMergePlaylists).then(function(values){
    //return (values);
    var union = [... new Set([...values[0], ...values[1]])];
    unionuris = union.map(function(item) {
      return "spotify:track:"+item;
    })
    return unionuris;
    })
  }).then(function(unionuris) {
    spotifyApi.addTracksToPlaylist("3llqkp7ztxPUqxju1e6HX7", unionuris);
  });
}


/*function getPromiseClearPlaylist (playlistId) {
  return getPromiseSongTrackIdsFromPlaylistById(playlistId)
  .then(function(data) {
    dmap = data.map(function(item) {
      return "spotify:track:"+item;
    });
    spotifyApi.removeTracksFromPlaylist(playlistId, dmap);
  }).then(function(retvalue) {
    return retvalue;
  });

}*/


function getPromiseToMergePlaylistsFromDescriptionOfPlaylistById (playlistId) {
  return spotifyApi.getPlaylist(playlistId)
  .then(function(data){
    let jsonObj = $("<div>").html(data["description"])[0].textContent;
    return JSON.parse(jsonObj);
  }).then(function(jsonObj) {
    return jsonObj["p"];
  })
}

function getPromiseSongTrackIdsFromPlaylistById(playlistId) {
  return spotifyApi.getPlaylistTracks(playlistId,
    {"market":"DE","fields":"items(track.id)"}
  )
  .then(function(data) {
    return data["items"].map(function(d){
      return d["track"]["id"];
    });
  });
}
